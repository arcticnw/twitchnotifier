﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Json;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TwitchNotifier
{
  public class Streamer
  {
    public enum StreamerStatus
    {
      /// <summary>
      /// The status has not been checked yet.
      /// </summary>
      Default,
      /// <summary>
      /// An error occured while retrieving the status or invalid status was reported.
      /// </summary>
      Error,
      Online,
      Offline,
    }

    /// <summary>
    /// Id of this application required to make requests to twitch api
    /// </summary>
    private const string CLIENT_ID = "mw8muhx47yhutj0w1jbrazbn9m4jwzo";

    /// <summary>
    /// Format of the request for channel information. First parameter is the channel's name.
    /// </summary>
    private const string REQUEST_CHANNEL_FORMAT = "https://api.twitch.tv/kraken/streams/{0}";
    /// <summary>
    /// Format of the request for channel hosting information. First parameter is the host's id.
    /// </summary>
    private const string REQUEST_HOSTING_FORMAT = "http://tmi.twitch.tv/hosts?include_logins=1&host={0}";
    private const string REQUEST_ACCEPT = "application/vnd.twitchtv.v3+json";

    public string Name { get; private set; }

    private DateTime lastChecked;

    public string Game { get; private set; }
    public StreamerStatus Status { get; private set; }

    public string LastGame { get; private set; }
    public StreamerStatus LastStatus { get; private set; }

    public Streamer(string name)
    {
      this.Name = name;
    }

    /// <summary>
    /// Requests current status of streamer from the server
    /// </summary>
    public void UpdateStatus()
    {
      LastStatus = Status;
      LastGame = Game;
      FetchAndParse();
    }
    public void Reset()
    {
      LastStatus = Status = StreamerStatus.Default;
      LastGame = Game = "";
    }

    /// <summary>
    /// Makes a request to the server and returns json data as dynamic type
    /// </summary>
    private dynamic FetchData(string request)
    {
      HttpWebRequest streamWebRequest = (HttpWebRequest)WebRequest.Create(request);
      streamWebRequest.Accept = REQUEST_ACCEPT;
      streamWebRequest.Headers.Add("Client-ID", CLIENT_ID);

      JsonObject jsonData = null;
      try
      {
        var responseStream = streamWebRequest.GetResponse();
        jsonData = JsonValue.Load(responseStream.GetResponseStream()).ToJsonObject();
      }
      catch (Exception ex)
      {
        Debug.WriteLine(ex.Message);
        return null;
      }

      return jsonData.AsDynamic();
    }
    /// <summary>
    /// Requests and processes the information from servers
    /// </summary>
    private void FetchAndParse()
    {
      lastChecked = DateTime.Now;
      Game = "";

      // Get channel data
      var data = FetchData(string.Format(REQUEST_CHANNEL_FORMAT, Name));
      if (data == null)
      {
        Status = StreamerStatus.Error;
        return;
      }

      /* the response looks like this:
        {
          "streams": [
            {
              "game": "StarCraft II: Heart of the Swarm",
              "is_playlist": false,
              "channel": {
                "_id": 12345,
              }
            },
          ]
        }
      */

      //

      // Not streaming ?
      if (data.stream.Count == 0)
      {
        Status = StreamerStatus.Offline;
        return;
      }

      // Playing playlist ?
      if ((bool)data.stream.is_playlist)
      {
        Status = StreamerStatus.Offline;
        return;
      }

      // Get hosting information
      long id = data.stream.channel._id;
      var dataHosting = FetchData(string.Format(REQUEST_HOSTING_FORMAT, id));
      if (dataHosting == null)
      {
        Status = StreamerStatus.Error;
        return;
      }

      /* the response looks like this:
        {
          "hosts": [
            {
              "target_id": 12345,
            },
          ]
        }
      */

      // Is hosting someone else's channel ?
      dynamic channelData = dataHosting.AsDynamic();
      if (channelData.hosts[0].target_id.JsonType != JsonType.Default)
      {
        //Status = StreamerStatus.Hosting;
        Status = StreamerStatus.Offline;
        return;
      }

      //

      Game = (string)data.stream.game;

      Status = StreamerStatus.Online;
    }
  }
}
