﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchNotifier
{
  /// <summary>
  /// Class contains settings for the twitch notifier
  /// </summary>
  public class Settings
  {
    /// <summary>
    /// Specifies for how long the toast notification should remain active (in seconds) or -1 to remain active until dismissed
    /// </summary>
    public int ToastDelay
    {
      get { return toastDelay; }
      set
      {
        Debug.Assert(value == -1 || value > 0);
        toastDelay = value;
      }
    }
    private int toastDelay;

    /// <summary>
    /// Specifies the interval between each checks (in minutes)
    /// </summary>
    public int CheckDelay
    {
      get { return checkDelay; }
      set
      {
        Debug.Assert(value > 0);
        checkDelay = value;
      }
    }
    private int checkDelay;

    public bool UseLivestreamer { get; set; }
    public string OAuthToken { get; set; }
    public string LivestreamerQuality { get; set; }

    /// <summary>
    /// Specifies what command should be executed when link in the toast notification is clicked (the link to the stream will be passed as parameter)
    /// </summary>
    public string CommandPrefix { get; set; }

    /// <summary>
    /// Specifies whether change of game should trigger toast notification
    /// </summary>
    public bool GameChangeTrigger { get; set; }

    public List<string> Channels { get; set; }

    public Settings()
    {
      ToastDelay = 10;
      CheckDelay = 2;

      UseLivestreamer = true;
      OAuthToken = "";
      LivestreamerQuality = "medium,best";
      CommandPrefix = "chrome";

      GameChangeTrigger = false;
      Channels = new List<string>();
    }

    /// <summary>
    /// Load settings from AppData
    /// </summary>
    /// <returns></returns>
    public static Settings Load()
    {
      Settings settings = new Settings();

      settings.ToastDelay = Properties.Settings.Default.toastDelay;
      settings.CheckDelay = Properties.Settings.Default.checkDelay;

      settings.UseLivestreamer = Properties.Settings.Default.livestreamer;
      settings.CommandPrefix = Properties.Settings.Default.commandPrefix;
      settings.LivestreamerQuality = Properties.Settings.Default.livestreamerQuality;

      settings.OAuthToken = Properties.Settings.Default.oauth_token;
      settings.GameChangeTrigger = Properties.Settings.Default.gameChangeTrigger;

      if (Properties.Settings.Default.channels != null)
        foreach (var item in Properties.Settings.Default.channels)
          settings.Channels.Add(item);

      return settings;
    }

    /// <summary>
    /// Save settings to AppData
    /// </summary>
    public void Save()
    {
      Properties.Settings.Default.toastDelay = ToastDelay;
      Properties.Settings.Default.checkDelay = CheckDelay;
      Properties.Settings.Default.livestreamer = UseLivestreamer;
      Properties.Settings.Default.commandPrefix = CommandPrefix;
      Properties.Settings.Default.livestreamerQuality = LivestreamerQuality;
      Properties.Settings.Default.gameChangeTrigger = GameChangeTrigger;
      Properties.Settings.Default.oauth_token = OAuthToken;

      var sc = new System.Collections.Specialized.StringCollection();
      foreach (string item in Channels)
        sc.Add(item);
      Properties.Settings.Default.channels = sc;

      Properties.Settings.Default.Save();
    }
  }
}
