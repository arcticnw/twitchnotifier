﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Windows.Threading;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace TwitchNotifier
{
  /// <summary>
  /// Interaction logic for Toast.xaml
  /// </summary>
  public partial class Toast : Window
  {
    private Settings settings;

    private CancellationTokenSource cts = new CancellationTokenSource();

    public Toast(Settings settings)
    {
      InitializeComponent();

      this.settings = settings;
    }

    /// <summary>
    /// Populates the infobox with report on given list of streamers, then resizes the box and readjusts the position of the box
    /// </summary>
    /// <param name="streamers"></param>
    public void MakeReport(List<Streamer> streamers)
    {
      infoBox.Text = "";

      foreach (var streamer in streamers)
      {
        // Create link text

        var command = new OpenStreamCommand()
        {
          Streamer = streamer,
          Settings = settings
        };
        var linktext = new Run(streamer.Name) { FontWeight = FontWeights.Bold };
        var link = new Hyperlink(linktext) { Command = command };

        infoBox.Inlines.Add(link);
        infoBox.Inlines.Add(" is ");

        // Create status text

        Brush b = Brushes.Black;
        switch (streamer.Status)
        {
          case Streamer.StreamerStatus.Online:
            b = Brushes.DarkGreen;
            break;
          case Streamer.StreamerStatus.Offline:
            b = Brushes.DarkRed;
            break;
          case Streamer.StreamerStatus.Error:
            b = Brushes.OrangeRed;
            break;
        }

        var statusText = new Run(streamer.Status.ToString()) { FontWeight = FontWeights.Bold, Foreground = b };

        if (streamer.Status != streamer.LastStatus)
        {
          infoBox.Inlines.Add("*");
          infoBox.Inlines.Add(statusText);
          infoBox.Inlines.Add("*");
        }
        else
        {
          infoBox.Inlines.Add(statusText);
        }

        // Create game details text

        if (streamer.Status == Streamer.StreamerStatus.Online)
        {
          infoBox.Inlines.Add(" playing ");

          var gameText = new Run(streamer.Game) { FontStyle = FontStyles.Italic };

          if (streamer.Game != streamer.LastGame)
          {
            infoBox.Inlines.Add("*");
            infoBox.Inlines.Add(gameText);
            infoBox.Inlines.Add("*");
          }
          else
          {
            infoBox.Inlines.Add(gameText);
          }
        }
        infoBox.Inlines.Add(new LineBreak());
      }

      Height = 90 + 24 * (streamers.Count() - 1);

      ReapplyLocation();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      ReapplyLocation();
    }

    /// <summary>
    /// Opens a window and returns without waiting for the newly opened window to close; starts a timer which will hide
    /// the window upon expiring (as specified in Settings.ToastDelay). If the delay value is zero or negative, the window will
    /// stay open.
    /// </summary>
    public new void Show()
    {
      cts.Cancel();

      cts = new CancellationTokenSource();

      base.Show();

      // Only run the delay timer if positive duration is specified
      if (settings.ToastDelay > 0)
      {
        Task.Factory.StartNew(() => HideAfter(settings.ToastDelay * 1000, cts.Token));
      }
    }

    private async void HideAfter(int time, CancellationToken ct)
    {
      try
      {
        await Task.Delay(time, ct);
      }
      catch (OperationCanceledException)
      {
        return;
      }

      Dispatcher.Invoke(Hide);
    }

    /// <summary>
    /// Readjust location of the window to fit the infobox in bottom right corner of the screen
    /// </summary>
    private void ReapplyLocation()
    {
      var workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
      var corner = new Point(workingArea.Right, workingArea.Bottom);

      this.Left = corner.X - this.ActualWidth;
      this.Top = corner.Y - this.ActualHeight;
    }

    private void Window_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      if (e.ChangedButton != MouseButton.Left)
        this.Hide();
    }
  }
}
