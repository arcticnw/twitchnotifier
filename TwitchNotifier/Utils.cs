﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using IWshRuntimeLibrary;

namespace TwitchNotifier
{
  // Functions found on internet and modified to suit this project's needs
  public static class Utils
  {
    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll")]
    static extern int GetSystemMetrics(int smIndex);

    public const int SM_CXSCREEN = 0;
    public const int SM_CYSCREEN = 1;

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool GetWindowRect(IntPtr hWnd, out W32RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct W32RECT
    {
      public int Left;
      public int Top;
      public int Right;
      public int Bottom;
    }

    public static bool IsForegroundWindowFullScreen()
    {
      int scrX = GetSystemMetrics(SM_CXSCREEN),
          scrY = GetSystemMetrics(SM_CYSCREEN);

      IntPtr handle = GetForegroundWindow();
      if (handle == IntPtr.Zero) return false;

      W32RECT wRect;
      if (!GetWindowRect(handle, out wRect)) return false;

      return scrX == (wRect.Right - wRect.Left) && scrY == (wRect.Bottom - wRect.Top);
    }

    public static void CreateShortcut(string shortcutPath, string shortcutTarget, string shortcutIconPath, string args, string workingDir)      
    {
      WshShell shell = new WshShell();
      IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPath);

      shortcut.IconLocation = shortcutIconPath;
      shortcut.TargetPath = shortcutTarget;
      shortcut.Arguments = args;
      shortcut.WorkingDirectory = workingDir;
      shortcut.Save();
    }
  }
}
