﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TwitchNotifier
{
  class OpenStreamCommand : ICommand
  {
    /// <summary>
    /// Twitch channel formatting. First parameter is the stream name.
    /// </summary>
    private const string TWITCH_CHANNEL_FORMAT = "twitch.tv/{0}";
    /// <summary>
    /// Parameters formatting for the livestreamer. First parameter is OAuth token, second is link to the channel and last one is stream quality
    /// </summary>
    private const string LIVESTREAMER_PARAM_FORMAT = "/c livestreamer --twitch-oauth-token {0} {1} {2}";

    /// <summary>
    /// The command is rebuilt every time check is performed and check value is unchanged when the toast is active, therefore this event isn't used
    /// </summary>
    public event EventHandler CanExecuteChanged;

    public Streamer Streamer { get; set; }
    public Settings Settings { get; set; }

    /// <summary>
    /// Allow execution only when the streamer is online
    /// </summary>
    public bool CanExecute(object parameter)
    {
      return Streamer.Status == Streamer.StreamerStatus.Online;
    }

    /// <summary>
    /// Open livestreamer, website or run custom command - depending on the settings
    /// </summary>
    public void Execute(object parameter)
    {
      string channel = string.Format(TWITCH_CHANNEL_FORMAT, Streamer.Name);

      if (Settings.UseLivestreamer)
      {
        System.Diagnostics.Process p = new System.Diagnostics.Process();

        p.StartInfo.FileName = "cmd";
        p.StartInfo.Arguments = string.Format(LIVESTREAMER_PARAM_FORMAT, Settings.OAuthToken, channel, Settings.LivestreamerQuality);
        p.StartInfo.UseShellExecute = true;

        p.Start();
      }
      else
      {
        if (string.IsNullOrWhiteSpace(Settings.CommandPrefix))
        {
          System.Diagnostics.Process.Start(@"https:\\www." + channel);
        }
        else
        {
          System.Diagnostics.Process p = new System.Diagnostics.Process();

          p.StartInfo.FileName = Settings.CommandPrefix;
          p.StartInfo.Arguments = channel;
          p.StartInfo.UseShellExecute = true;

          p.Start();
        }
      }
    }
  }
}
