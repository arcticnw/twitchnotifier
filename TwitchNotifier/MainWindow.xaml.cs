﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;

namespace TwitchNotifier
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private Toast toastWindow;
    private NotifyIcon notifyIcon;

    private CancellationTokenSource loopCancelTokenSource;

    private List<Streamer> streamers = new List<Streamer>();

    private Settings settings;

    public MainWindow()
    {
      InitializeComponent();
      settings = Settings.Load();
    }

    private void CreateTrayIcon()
    {
      notifyIcon = new NotifyIcon();
      notifyIcon.Visible = true;
      notifyIcon.Icon = new System.Drawing.Icon("favicon.ico");
      notifyIcon.Text = "Twitch notifier";

      //notifyIcon.Click += (_, __) => Show();

      notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] {
        new MenuItem("Check now", checkNowTray_Click),
        new MenuItem("Show last", showLastTray_Click),
        new MenuItem("Open config", openConfigTray_Click),
        new MenuItem("Close", closeTray_Click),
      });
    }

    private void ParseCommandLineArguments()
    {
      string[] minimizeArgs = new string[] { "minimized", "minimize", "min", "m" };
      bool minimize = false;

      string[] args = Environment.GetCommandLineArgs();

      // Parse arguments
      for (int i = 0; i < args.Length; i++)
      {
        string argument = args[i];
        if (argument.Length < 1) continue;
        if (argument[0] == '/' || argument[0] == '-')
          argument = argument.Substring(1);

        if (minimizeArgs.Contains(argument))
          minimize = true;
      }

      // Apply arguments
      if (minimize)
      {
        Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(() => runButton_Click(null, null)));
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      CreateTrayIcon();

      toastWindow = new Toast(settings);
      loopCancelTokenSource = new CancellationTokenSource();

      ApplySettingsToUI();

      ParseCommandLineArguments();
    }

    /// <summary>
    /// Functions that need to be called before the Dispose method of this window
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      settings.Save();
      toastWindow.Close();
      notifyIcon.Visible = false;
      notifyIcon.Dispose();
      loopCancelTokenSource.Cancel();
      loopCancelTokenSource.Dispose();
    }

    /// <summary>
    /// Runs the check immediately and restarts the wait loop
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void checkNowTray_Click(object sender, EventArgs e)
    {
      loopCancelTokenSource.Cancel();
      DoCheck(true, new CancellationToken(false));

      if (!IsVisible)
        StartLoop();
    }
    /// <summary>
    /// Displays the last toast notification without running the check
    /// </summary>
    private void showLastTray_Click(object sender, EventArgs e)
    {
      CreatePopup(true, new CancellationToken(false));
    }
    /// <summary>
    /// Opens the config window and bumps the window to foreground
    /// </summary>
    private void openConfigTray_Click(object sender, EventArgs e)
    {
      Show();

      // Bump the config window to foreground and focus on it
      if (WindowState == WindowState.Minimized)
        WindowState = WindowState.Normal;
      Activate();
      Topmost = true;
      Topmost = false;
      Focus();
    }
    private void closeTray_Click(object sender, EventArgs e)
    {
      Close();
    }

    /// <summary>
    /// Starts a test check on the currently watched channels; does not hide window, does not start the loop
    /// </summary>
    private void testButton_Click(object sender, RoutedEventArgs e)
    {
      ClearInfo();
      ApplySettingsFromUI();

      DoCheck(true, new CancellationToken(false));
    }
    /// <summary>
    /// Hides the config window and starts the main loop which will periodically run the check
    /// </summary>
    private void runButton_Click(object sender, RoutedEventArgs e)
    {
      ClearInfo();
      ApplySettingsFromUI();

      Hide();

      StartLoop();
    }
    /// <summary>
    /// Creates a shortcut to this application in the startup folder
    /// </summary>
    private void startupButton_Click(object sender, RoutedEventArgs e)
    {
      var startupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
      var shortcutPath = Path.Combine(startupPath, "TwitchNotifier.lnk");
      var shortcut = new FileInfo(shortcutPath);

      var wd = new DirectoryInfo(".").FullName;

      string target = System.Reflection.Assembly.GetExecutingAssembly().Location;
      string icon = Path.Combine(wd, "favicon.ico");

      if (shortcut.Exists)
        shortcut.Delete();

      Utils.CreateShortcut(shortcutPath, target, icon, "-m", wd);
    }
    private void closeButton_Click(object sender, RoutedEventArgs e)
    {
      this.Close();
    }

    private void addChannel_Click(object sender, RoutedEventArgs e)
    {
      if (string.IsNullOrWhiteSpace(streamerNameText.Text))
        return;

      foreach (string item in listBox.Items)
        if (item == streamerNameText.Text)
          return;

      listBox.Items.Add(streamerNameText.Text);
      streamerNameText.Text = "";

      UpdateStreamerList();
    }
    private void listBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    {
      if (listBox.SelectedIndex == -1) return;
      if (e.Key != Key.Delete) return;

      listBox.Items.RemoveAt(listBox.SelectedIndex);
      UpdateStreamerList();
    }

    private void livestreamerCheckbox_Checked(object sender, RoutedEventArgs e)
    {
      bool useLivestreamer = livestreamerCheckbox.IsChecked.Value;

      if (useLivestreamer)
      {
        livestreamerOptions.Visibility = Visibility.Visible;
        browserOptions.Visibility = Visibility.Collapsed;
      }
      else
      {
        livestreamerOptions.Visibility = Visibility.Collapsed;
        browserOptions.Visibility = Visibility.Visible;
      }
    }
    private void gameChangedTrigger_Click(object sender, RoutedEventArgs e)
    {

    }

    /// <summary>
    /// Opens the window; stops the check loop if active
    /// </summary>
    public new void Show()
    {
      // When the config window is active, stop the check loop
      loopCancelTokenSource.Cancel();
      base.Show();
    }

    /// <summary>
    /// Starts the check loop
    /// </summary>
    private void StartLoop()
    {
      // Abort existing loop
      loopCancelTokenSource.Cancel();

      // Start new loop
      loopCancelTokenSource = new CancellationTokenSource();
      var ct = loopCancelTokenSource.Token;

      Task.Factory.StartNew(async () =>
      {
        try
        {
          while (true)
          {
            // CheckDelay is in minutes, so *60*1000 to get miliseconds
            await Task.Delay(settings.CheckDelay * 60 * 1000, ct);

            // Run check
            DoCheck(false, ct);
          }
        }
        catch (OperationCanceledException)
        { }
      });
    }

    private void ApplySettingsFromUI()
    {
      settings.UseLivestreamer = livestreamerCheckbox.IsChecked.Value;
      settings.LivestreamerQuality = livestreamerQuality.Text;
      settings.OAuthToken = oauthToken.Text;

      settings.GameChangeTrigger = gameChangedTrigger.IsChecked.Value;

      settings.CommandPrefix = commandPrefix.Text;

      // toast delay
      int newToastDelay;
      if (!int.TryParse(toastDelay.Text, out newToastDelay))
      {
        newToastDelay = settings.ToastDelay;
        toastDelay.Text = settings.ToastDelay.ToString();
      }
      if (newToastDelay < -1 || newToastDelay == 0)
      {
        newToastDelay = -1;
        toastDelay.Text = newToastDelay.ToString();
      }
      settings.ToastDelay = newToastDelay;

      // check delay
      int newCheckDelay = 0;
      if (!int.TryParse(this.checkDelay.Text, out newCheckDelay) || newCheckDelay < 1)
      {
        newCheckDelay = settings.CheckDelay;
        checkDelay.Text = settings.CheckDelay.ToString();
      }
      settings.CheckDelay = newCheckDelay;

      //

      UpdateStreamerList();
    }
    private void ApplySettingsToUI()
    {
      toastDelay.Text = settings.ToastDelay.ToString();
      checkDelay.Text = settings.CheckDelay.ToString();

      livestreamerCheckbox.IsChecked = settings.UseLivestreamer;
      commandPrefix.Text = settings.CommandPrefix;
      livestreamerQuality.Text = settings.LivestreamerQuality;

      oauthToken.Text = settings.OAuthToken;

      gameChangedTrigger.IsChecked = settings.GameChangeTrigger;

      foreach (var item in settings.Channels)
        listBox.Items.Add(item);

      if (settings.UseLivestreamer)
      {
        livestreamerOptions.Visibility = Visibility.Visible;
        browserOptions.Visibility = Visibility.Collapsed;
      }
      else
      {
        livestreamerOptions.Visibility = Visibility.Collapsed;
        browserOptions.Visibility = Visibility.Visible;
      }
    }

    /// <summary>
    /// Updates the list of watched channels
    /// </summary>
    private void UpdateStreamerList()
    {
      settings.Channels = listBox.Items.Cast<string>().ToList();

      HashSet<string> oldSet = new HashSet<string>(streamers.Select(p => p.Name));
      HashSet<string> newSet = new HashSet<string>(settings.Channels);

      HashSet<string> toRemove = new HashSet<string>(oldSet);
      toRemove.ExceptWith(newSet);

      HashSet<string> toAdd = new HashSet<string>(newSet);
      toAdd.ExceptWith(oldSet);

      streamers.RemoveAll(p => toRemove.Contains(p.Name));

      foreach (var item in toAdd)
        streamers.Add(new Streamer(item));
    }

    /// <summary>
    /// Updates the status of all watched channels
    /// </summary>
    /// <param name="ct">Cancellation token for this operation</param>
    private void UpdateStreamerStatus(CancellationToken ct)
    {
      Dispatcher.Invoke(() => SuspendControls(true));

      foreach (var item in streamers)
      {
        item.UpdateStatus();
        if (ct.IsCancellationRequested)
          return;
      }

      Dispatcher.Invoke(() => SuspendControls(false));
    }

    private void SuspendControls(bool suspend)
    {
      addChannelButton.IsEnabled = !suspend;
      testButton.IsEnabled = !suspend;
      runButton.IsEnabled = !suspend;
    }

    private void ClearInfo()
    {
      foreach (var item in streamers)
      {
        item.Reset();
      }
    }
    /// <summary>
    /// Filters out inactive streamers and checks changes. If changes occured opens the toast notification with active streamers and updates tooltip on 
    /// tray icon. The toast window will not be opened if a fullscreen application is currently running in foreground.
    /// </summary>
    /// <param name="fullReport">If set to true, function will skip filtering and checking and will display all streamers.</param>
    /// <param name="ct">Cancellation token for this operation</param>
    private void CreatePopup(bool fullReport, CancellationToken ct)
    {
      // Create list of active streamers

      List<Streamer> report = new List<Streamer>();
      bool changed = false;
      foreach (var item in streamers)
      {
        if (!fullReport)
        {
          // if status is not online, skip the channel
          if (item.Status != Streamer.StreamerStatus.Online)
            continue;

          // if last status wasn't online last time, mark change
          if (item.LastStatus != Streamer.StreamerStatus.Online)
            changed = true;

          // if the game changed
          if (settings.GameChangeTrigger && item.LastGame != item.Game)
            changed = true;
        }

        report.Add(item);
      }

      if (ct != null)
        ct.ThrowIfCancellationRequested();

      // Stop if there's nothing to display

      if (report.Count == 0 || (!changed && !fullReport))
        return;

      // Update tray icon tooltip

      StringBuilder sb = new StringBuilder();
      bool addSep = false;

      foreach (var item in report.Where(p => p.Status == Streamer.StreamerStatus.Online))
      {
        if (addSep)
          sb.Append(", ");
        sb.Append(item.Name.Substring(0, item.Name.Length > 6 ? 6 : item.Name.Length));
        addSep = true;
      }

      string notifyText;

      if (sb.Length == 0)
        notifyText = "TwitchNotifier: -";
      else
        notifyText = "TwitchNotifier: " + sb.ToString();

      // Tooltip can't be longet than 64 characters
      if (notifyText.Length >= 64)
        notifyText = notifyText.Substring(0, 60) + "...";

      // Update tooltip
      Dispatcher.Invoke(() => notifyIcon.Text = notifyText);

      // Is fullscreen application active?
      if (Utils.IsForegroundWindowFullScreen())
        return;

      // Create the toast notification
      Dispatcher.Invoke(() =>
      {
        toastWindow.MakeReport(report);
        toastWindow.Show();
      });
    }

    /// <summary>
    /// Updates information about watched channels and shows toast notification with information if changes occured.
    /// </summary>
    /// <param name="fullReport">If set to true, all streamers will be shown regardless of status and changes.</param>
    /// <param name="ct">Cancellation token for this operation</param>
    private void DoCheck(bool fullReport, CancellationToken ct)
    {
      // Is fullscreen application active?
      if (Utils.IsForegroundWindowFullScreen())
        return;

      UpdateStreamerStatus(ct);

      CreatePopup(fullReport, ct);
    }

    private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
    {
      System.Diagnostics.Process.Start(e.Uri.ToString());
    }
  }
}
