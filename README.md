Twitch notifier

This application periodically checks Twitch.tv servers for updates of given set of channels. Should any channel go from offline to online a notification displays in bottom right corner of the screen.